(ns plf02.core)

;Bloque de Funciones para associative?

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 3)
(función-associative?-2 [2 4 8])
(función-associative?-3 {:k1 '(z x w) :k2 [8 7 6] :k3 #{\a \b \c}})

;Bloque de Funciones para boolean?

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 (< 7 8))
(función-boolean?-2 nil)
(función-boolean?-3 true)

;Bloque de Funciones para char?

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 "M")
(función-char?-2 \o)
(función-char?-3 (first "Natali"))

;Bloque de Funciones para coll?

(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 #{"Mar" "Ola" "práctica"})
(función-coll?-2 [\l 23 false "nombre" '(9 8 7) {:k1 3 :k2 4}])
(función-coll?-3 "String")

;Bloque de Funciones para decimal?

(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 12345)
(función-decimal?-2 7.1435M)
(función-decimal?-3 -0.96)

;Bloque de Funciones para double?

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 -0.6789)
(función-double?-2 824.9765M)
(función-double?-3 824)

;Bloque de Funciones para float?

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 2.5)
(función-float?-2 824.9765M)
(función-float?-3 13.54)

;Bloque de Funciones para ident?

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 "p")
(función-ident?-2 :key)
(función-ident?-3 'simbolo)

;Bloque de Funciones para indexed?

(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [\a \b \c \d])
(función-indexed?-2 '(1 2 3))
(función-indexed?-3 ['(9 2 3) #{true false} [1 2 3]])

;Bloque de Funciones para int?

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 18908)
(función-int?-2 1)
(función-int?-3 123.00)

;Bloque de Funciones para integer?

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 18908N)
(función-integer?-2 1)
(función-integer?-3 123.00)

;Bloque de Funciones para keyword?

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 :keyword)
(función-keyword?-2 :clave)
(función-keyword?-3 'noesKey)

;Bloque de Funciones para list?

(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 #{\a \f \t})
(función-list?-2 '(98 34 23))
(función-list?-3 (list 999 998 997))

;Bloque de Funciones para map-entry?

(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 {:t1 :t2 :t3 :t4})
(función-map-entry?-2 (find {:key1 \f :key2 \s} :key2))
(función-map-entry?-3 (first {:clave1 '(true false) :clave2 ['d 't 'w] :clave3 #{"PLF"}}))

;Bloque de Funciones para map?

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 {:t1 :t2 :t3 :t4})
(función-map?-2 (hash-map :a :b :c :d))
(función-map?-3 [{:k :w :t :c} '(1 2 3)])

;Bloque de Funciones para nat-int

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 3/4)
(función-nat-int?-2 1)
(función-nat-int?-3 -88)

;Bloque de Funciones para number?

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 3.5M)
(función-number?-2 3456N)
(función-number?-3 {:a -88})

;Bloque de Funciones para pos-int?

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 1980)
(función-pos-int?-2 1)
(función-pos-int?-3 -0.25)

;Bloque de Funciones para ratio?

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

(función-ratio?-1 3/4)
(función-ratio?-2 0.25)
(función-ratio?-3 22/12)

;Bloque de Funciones para rational?

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 3/4)
(función-rational?-2 123)
(función-rational?-3 0.43)

;Bloque de Funciones para seqable?

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 "Mareli")
(función-seqable?-2 [6 4 2])
(función-seqable?-3 0.43)

;Bloque de Funciones para sequential?

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

(función-sequential?-1 "Mareli")
(función-sequential?-2 (list "z" "x" "y" "w"))
(función-sequential?-3 (vector 9 8 7 6 5 4 3 2 1 0))

;Bloque de Funciones para set?

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 [\a \b \c \d])
(función-set?-2 #{10 1 345 9876})
(función-set?-3 (hash-set 45 46 47))

;Bloque de Funciones para some?


(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 nil)
(función-some?-2 's)
(función-some?-3 true)

;Bloque de Funciones para string?

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "Programación Funcional")
(función-string?-2 "")
(función-string?-3 \m)

;Bloque de Funciones para symbol?

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 's)
(función-symbol?-2 "M")
(función-symbol?-3 'simbolo)

;Bloque de Funciones para vector?

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 (vec '(false true true)))
(función-vector?-2 [[3 4] [9 8 7] [4 6 3 2]])
(función-vector?-3 #{\a \b \c})

;Bloque de Funciones para drop


(defn función-drop-1
  [x y]
  (drop x y))

(defn función-drop-2
  [x y]
  (drop x y))

(defn función-drop-3
  [x]
  (drop 2 (assoc x 2 3)))

(función-drop-1 2 [2 3 4 5 6])
(función-drop-2 2 ['(\a \b) #{'d 'g 'w} 234 [23 45]])
(función-drop-3  [[23] [24] [25] [26]])

;Bloque de Funciones para drop-last

(defn función-drop-last-1
  [x]
  (drop-last x))

(defn función-drop-last-2
  [x y]
  (drop-last x y))

(defn función-drop-last-3
  [x y]
  (drop-last x y))

(función-drop-last-1 '(2 3 4 5 6))
(función-drop-last-2 3 [true false '(\a \b) #{'d 'g 'w} 234 [23 45]])
(función-drop-last-3 2 [[23] [24] [25] [26]])

;Bloque de Funciones para drop-while

(defn función-drop-while-1
  [x y]
  (drop-while x y))

(defn función-drop-while-2
  [x y]
  (drop-while x y))

(defn función-drop-while-3
  [w xs]
  (drop-while w xs))

(función-drop-while-1 char? '(\a \b \c \d 'e \f 'g \r))
(función-drop-while-2 pos? [23 456 -45 -56 1 2 3])
(función-drop-while-3 int #{-2 3.5 -5 6 8.1 -9 10 11})

;Bloque de Funciones para every?

(defn función-every?-1
  [x y]
  (every? x y))

(defn función-every?-2
  [x y]
  (every? x y))

(defn función-every?-3
  [y xs]
  (every? y xs))

(función-every?-1 {:k1 3 :k2 4} [:k1 :2])
(función-every?-2 keyword? [:k1 :k2 :k3 23 'b])
(función-every?-3 number? '(2 3 4 5))


;Bloque de Funciones para filterv

(defn función-filterv-1
  [y x]
  (filterv y x))

(defn función-filterv-2
  [y x]
  (filterv y x))

(defn función-filterv-3
  [y xs]
  (filterv y xs))

(función-filterv-1 string? ["Mareli" "Práctica" "Funciones"])
(función-filterv-2 vector? {})
(función-filterv-3 float? [2.3 3 4.8 5])

;Bloque de Funciones para group-by

(defn función-group-by-1
  [n m]
  (group-by n m))

(defn función-group-by-2
  [x y]
  (group-by  x y))

(defn función-group-by-3
  [y xs]
  (group-by y xs))

(función-group-by-1 string? ["Mareli" "Práctica" "Funciones"])
(función-group-by-2 flatten '(10 20 [3 (4 5)]))
(función-group-by-3 count [(keys  {:key1 23 :key2 87 :key3 21})])

;Bloque de Funciones para iterate

(defn función-iterate-1
  [x]
  (iterate dec x))

(defn función-iterate-2
  [x y]
  (iterate x y))

(defn función-iterate-3
  [x y]
  (iterate x y))

(función-iterate-1 290)
(función-iterate-2 inc 196798)
(función-iterate-3 dec 28)

;Bloque de Funciones para keep

(defn función-keep-1
  [x y]
  (keep x y))

(defn función-keep-2
  [x y]
  (keep x y))

(defn función-keep-3
  [x y]
  (keep x y))

(función-keep-1 ratio? [1/2 2.5 12/3 1 2 3 3/4])
(función-keep-2  indexed? ['() [] #{} {}])
(función-keep-3 ident? (keys {:k1 4}))

;Bloque de Funciones para keep-indexed
;
(defn función-keep-indexed-1
  [x y]
  (keep-indexed x y))

(defn función-keep-indexed-2
  [x y]
  (keep-indexed x y))

(defn función-keep-indexed-3
  [x y]
  (keep-indexed x y))

(función-keep-indexed-1 {} [-9 0 29 -7 45 3 -8])
(función-keep-indexed-2 vector "Práctica PLF")
(función-keep-indexed-3 {} "Natali")

;Bloque de Funciones para map-indexed

(defn función-map-indexed-1
  [x y]
  (map-indexed x y))

(defn función-map-indexed-2
  [x y]
  (map-indexed x y))

(defn función-map-indexed-3
  [x y]
  (map-indexed x y))

(función-map-indexed-1 hash-map "Programación")
(función-map-indexed-2 vector "idea")
(función-map-indexed-3 list [1 2 3 4 6])

;Bloque de Funciones para mapcac

(defn función-mapcat-1
  [a x n]
  (mapcat a x n))

(defn función-mapcat-2
  [x w y]
  (mapcat x w y))

(defn función-mapcat-3
  [x y w]
  (mapcat x y w))

(función-mapcat-1 list [:a :b :c] [13 22 35])
(función-mapcat-2 list ['s 'v 't] [\a \b \c])
(función-mapcat-3 vector [:k1 :k2 :k3] ["j" "n" "l" "p"])

;Bloque de Funciones para mapv

(defn función-mapv-1
  [x y]
  (mapv x y))

(defn función-mapv-2
  [x y]
  (mapv x y))

(defn función-mapv-3
  [x y w]
  (mapv x y w))

(función-mapv-1 inc [89 45 100 2])
(función-mapv-2 dec [4 38 293 4 12 34 566])
(función-mapv-3 + [9 8 7 6] [1999 1998 1997 1996])

;Bloque de Funciones para merge-with

(defn función-merge-with-1
  [x y]
  (merge-with x y))

(defn función-merge-with-2
  [x y]
  (merge-with - x y))

(defn función-merge-with-3
  [x y w]
  (merge-with x y w))

(función-merge-with-1 inc {:k1 10  :k3 62})
(función-merge-with-2 {:k1 900  :k2 98 :k3 456} {:k1 910  :k2 48 :k3 56})
(función-merge-with-3 - {:a 9  :b 98  :c 0}  {:a 9  :b 98  :c 0})

;Bloque de Funciones para not-any?

(defn función-not-any?-1
  [x y]
  (not-any?  x y))

(defn función-not-any?-2
  [x y]
  (not-any? x y))

(defn función-not-any?-3
  [x y]
  (not-any? x y))

(función-not-any?-1 coll? '({} #{} [] '()))
(función-not-any?-2 nat-int? '(-890 890))
(función-not-any?-3 sequential? [1 2 3 4])


;Bloque de Funciones para not-every?

(defn función-not-every?-1
  [x y]
  (not-every? x y))

(defn función-not-every?-2
  [x y]
  (not-every? x y))

(defn función-not-every?-3
  [x y]
  (not-every? x y))

(función-not-every?-1 pos? (list 1 2 3 4 -3))
(función-not-every?-2 odd? [-894 892])
(función-not-every?-3 nil? nil)


;Bloque de Funciones para partition-by

(defn función-partition-by-1
  [x y]
  (partition-by x y))

(defn función-partition-by-2
  [x y]
  (partition-by x y))

(defn función-partition-by-3
  [x y]
  (partition-by x y))

(función-partition-by-1 identity "PEER")
(función-partition-by-2 odd? [-10 2 -9 15])
(función-partition-by-3 even? '(1 3 5 2 4 7 9 10 12 14 16))

;Bloque de Funciones para reduce-kv

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))


(función-reduce-kv-1 conj [] [[89 4 245] 84 3 8])
(función-reduce-kv-2 conj [] [[3479 3] [3 4 5 6] [3 4 5]])
(función-reduce-kv-1 conj [] [3 889 4 [45 67 8]])

;Bloque de Funciones para remove

(defn función-remove-1
  [x y]
  (remove x y))

(defn función-remove-2
  [x y]
  (remove x y))

(defn función-remove-3
  [x y]
  (remove x y))

(función-remove-1 char? '(\a \b 's 'c 23 \c))
(función-remove-2 pos-int? [4 5 7 8 -3 -677])
(función-remove-3 even? '(1 3 5 2 4 7 9 10 12 14 16))


;Bloque de Funciones para reverse

(defn función-reverse-1
  [x]
  (reverse x))

(defn función-reverse-2
  [x]
  (reverse x))

(defn función-reverse-3
  [x]
  (reverse x))

(función-reverse-1 "Estudiante")
(función-reverse-1 [\a \b [2 3 5] '('f 'h)])
(función-reverse-3 '({:j :k} #{3 5} [[3 4] [9 8]]))



;Bloque de Funciones para some

(defn función-some-1
  [x y]
  (some x y))

(defn función-some-2
  [x y]
  (some x y))

(defn función-some-3
  [x y]
  (some x y))

(función-some-1 int? [78 4 0.5 10 5.4 234N 0.4M])
(función-some-2 seq? ['(9 8 7) 25])
(función-some-3 some? [\f 'g true])


;Bloque de Funciones para sort-by

(defn función-sort-by-1
  [x xs]
  (sort-by x xs))

(defn función-sort-by-2
  [x y xs]
  (sort-by x y xs))

(defn función-sort-by-3
  [x y]
  (sort-by x y))

(función-sort-by-1 count [[78 4] [0.5] [10 5.4 234N] [0.4M 4 5 7]])
(función-sort-by-2 first < [[98] [1]])
(función-sort-by-3 count [[#{2 3 4}] [(list \v \c \s \g) (list 'a 'b) (list true false)] [9 8 7 5] [[2 3] [8 9 7]]])


;Bloque de Funciones para split-with

(defn función-split-with-1
  [x y]
  (split-with x y))

(defn función-split-with-2
  [x y]
  (split-with x y))

(defn función-split-with-3
  [x y]
  (split-with x y))

(función-split-with-1 associative? [[] (hash-map) '() #{} nil])
(función-split-with-2 boolean? ['(9 4 3) [83 2 4]])
(función-split-with-3  rational? [7/8 6.8 5 7 9.0 12.9])


;Bloque de Funciones para take

(defn función-take-1
  [x y]
  (take x y))

(defn función-take-2
  [x y]
  (take x y))

(defn función-take-3
  [x y]
  (take x y))

(función-take-1 2 #{34 95 23 5 73 2})
(función-take-2 1 [23 1998])
(función-take-3 5  ['b 's 'f {:d :e :r :f} '(3 4 5)])


;Bloque de Funciones para take-last

(defn función-take-last-1
  [x y]
  (take-last x y))

(defn función-take-last-2
  [x y]
  (take-last x y))

(defn función-take-last-3
  [x y]
  (take-last x y))

(función-take-last-1 3 #{34 95 23 5 73 2})
(función-take-last-2 1 [23 1998])
(función-take-last-3 2 ['b 's 'f {:d :e :r :f} '(3 4 5)])


;Bloque de Funciones para take-nth

(defn función-take-nth-1
  [x y]
  (take-nth x y))

(defn función-take-nth-2
  [x y]
  (take-nth x y))

(defn función-take-nth-3
  [x y]
  (take-nth x y))

(función-take-nth-1 3 '(3 27 5 9 17 3 57 23 1 2 9 12 15))
(función-take-nth-2 2 (range 2 18))
(función-take-nth-3 3 ['b \j 1235 's 'f {:d :e :r :f} '(3 4 5)])


;Bloque de Funciones para take-while

(defn función-take-while-1
  [x y]
  (take-while x y))

(defn función-take-while-2
  [x y]
  (take-while x y))

(defn función-take-while-3
  [x y]
  (take-while x y))

(función-take-while-1 pos? [3 4 5 -1 -5 -3])
(función-take-while-2 boolean? [true 1234])
(función-take-while-3 char? [\b \j 1235 \t 's '(3 4 5)])


;Bloque de Funciones para update

(defn función-update-1
  [x y w]
  (update x y w))

(defn función-update-2
  [x y w]
  (update x y w))

(defn función-update-3
  [x y w]
  (update x y w))

(función-update-1 {:nombre "Mareli Natali" :edad 20} :edad inc)
(función-update-2 [3 4 8 -1 -5 -3] 2 dec)
(función-update-3 [\b \j \t 1235 's '(3 4 5)] 3 dec)

;Bloque de Funciones para update-in

(defn función-update-in-1
  [x y z h]
  (update-in x y z h))

(defn función-update-in-2
  [x y s w h]
  (update-in x y s w h))

(defn función-update-in-3
  [a b c d e f g]
  (update-in a b c d e f g))

(función-update-in-1  [8 4 5 3 2 1] [3] * 10)

(función-update-in-2 {:nombre "Mareli Natali" :edad 35} [:edad] - 14 4)
(función-update-in-3 [\b \j 1235 \t 's '(3 4 5)] [2] * 30 5 3 9)




























